package db

import (
	"database/sql/driver"
	"sync"

	_ "github.com/jinzhu/gorm"
)

//database info
type DB struct {
	driver   driver.Driver
	dsn      string
	mu       sync.Mutex // protects freeConn and closed
	freeConn []driver.Conn
	closed   bool
}

const (
	DB_USER     = "postgres"
	DB_PASSWORD = "postgres"
	DB_NAME     = "test"
)

func init() {

}
