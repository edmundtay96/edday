package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"

	"github.com/miguelmota/go-ethereum-hdwallet"
	bip39 "github.com/tyler-smith/go-bip39"
)

type accdetails struct {
	Mnemonic string `json:"Mnemonic"`
	Address  string `json:"Address"`
}

func generateAccount() accdetails {
	// Generate a mnemonic for memorization or user-friendly seeds
	entropy, _ := bip39.NewEntropy(128)
	mnemonic, _ := bip39.NewMnemonic(entropy)

	// Generate a Bip32 HD wallet for the mnemonic and a user supplied password
	seed := bip39.NewSeed(mnemonic, "")

	wallet, _ := hdwallet.NewFromSeed(seed)
	path := hdwallet.MustParseDerivationPath("m/44'/60'/0'/0/0")
	account, _ := wallet.Derive(path, false)

	// Display mnemonic and keys
	//fmt.Println("Mnemonic: ", mnemonic)
	//fmt.Println(account.Address.Hex())
	return accdetails{Mnemonic: mnemonic, Address: account.Address.Hex()}

}

func bip() {
	count := 0
	for {

		re := regexp.MustCompile(`(?i)^0xeddea`)
		if temp := generateAccount(); re.MatchString(temp.Address) {
			tempJSON, _ := json.Marshal(temp)

			err := ioutil.WriteFile("accounts/golang/"+temp.Address+".json", tempJSON, 0644)

			fmt.Println("Mnemonic: ", temp.Mnemonic)
			fmt.Println("Address: ", temp.Address)
			if err != nil {
				fmt.Println(err)
			}
		}
		count++
		if count%1000 == 0 {
			fmt.Println("Total: ", count)
		}
	}
}
