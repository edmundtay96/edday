package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/alexedwards/scs"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/sirupsen/logrus"
)

//Person
///curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh for dep
//logrus for logging
//html/template for strings and return templates
//github.com/jinzhu/gorm for sql builder
type Person struct {
	ID        string   `json:"id,omitempty"`
	Firstname string   `json:"firstname,omitempty"`
	Lastname  string   `json:"lastname,omitempty"`
	Address   *Address `json:"address,omitempty"`
}

//Address demo
type Address struct {
	City  string `json:"city,omitempty"`
	State string `json:"state,omitempty"`
}

var sessionManager = scs.NewCookieManager("u46IpCV9y5Vlur8YvODJEhgOY8m9JVE4")

var people []Person

// our main function
func main() {

	router := chi.NewRouter()

	//cors
	cors := cors.New(cors.Options{
		// AllowedOrigins: []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	//middleware
	router.Use(middleware.RealIP)
	router.Use(cors.Handler)

	router.Use(middleware.RequestID)

	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(middleware.Timeout(60 * time.Second))

	router.HandleFunc("/", Login)
	router.HandleFunc("/favicon.ico", faviconHandler)
	router.HandleFunc("/js/{jsfile}", libHandler)

	router.Get("/transations", GetPeople)
	router.Get("/block={[0-9]}", GetPeople)
	router.Get("/test", Login)
	router.Post("/people/{id}", GetPerson)
	router.Post("/people/{id}", CreatePerson)
	router.Post("/people/{id}", DeletePerson)

	log.Fatal(http.ListenAndServe(":8000", router))
}

//javacript handlers
func libHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.URL.Path)
	http.ServeFile(w, r, "."+r.URL.Path)
}

//favicon
func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./favicon.ico")
}

//serve js files
func SendJqueryJs(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "jquery.min.js")
}

//Login
func Login(w http.ResponseWriter, r *http.Request) {
	crutime := time.Now().Unix()
	h := md5.New()
	io.WriteString(h, strconv.FormatInt(crutime, 10))
	//token := fmt.Sprintf("%x", h.Sum(nil))
	http.ServeFile(w, r, "index.html")
	//t, _ := template.ParseFiles("index.html")
	//t.Execute(w, token)
}

//GetPeople gets the people
func GetPeople(w http.ResponseWriter, r *http.Request) {
	//prevent excape strings
	name := template.HTMLEscapeString(r.Form.Get("name"))
	fmt.Printf(name)
	//cookie
	expiration := time.Now().Add(365 * 24 * time.Hour)
	cookie := http.Cookie{Name: "username", Value: "astaxie", Expires: expiration}
	http.SetCookie(w, &cookie)

	json.NewEncoder(w).Encode(people)

}

//GetPerson test
func GetPerson(w http.ResponseWriter, r *http.Request) {

}

//CreatePerson test
func CreatePerson(w http.ResponseWriter, r *http.Request) {}

//DeletePerson test
func DeletePerson(w http.ResponseWriter, r *http.Request) {}

//loggin handler
func loggingHandler(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		logrus.WithFields(logrus.Fields{
			"animal": "walrus",
		}).Info("People's data")
		t1 := time.Now()
		next.ServeHTTP(w, r)
		t2 := time.Now()
		log.Printf("[%s] %q %v", r.Method, r.URL.String(), t2.Sub(t1))
	}

	return http.HandlerFunc(fn)
}

func putHandler(w http.ResponseWriter, r *http.Request) {
	// Load the session data for the current request. Any errors are deferred
	// until you actually use the session data.
	session := sessionManager.Load(r)

	// Use the PutString() method to add a new key and associated string value
	// to the session data. Methods for many other common data types are also
	// provided. The session data is automatically saved.
	err := session.PutString(w, "message", "Hello world!")
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}
